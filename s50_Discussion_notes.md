React.js?

JavaScript Library for building user interfaces that is:

    Declarative: It makes you write code with the question of WHAT instead of HOW something is to be done. It also allows your code to be more predictable and easier to debug.
    
    Component-Based: Allows you to write reusable, complex UI components in a quick and efficient manner.
    Learn Once, Write Anywhere: React can also be used on the server using Node and power mobile apps using React Native

What Problem Does It Solve?

    Applications that require frequent and rapid changes in page data are resource-intensive and complicated to code.


How React.js Solves The Problem?

    Separate concerns into components rather than technologies instead of the conventional HTML for structure, CSS for styling, and JavaScript for interactivity

    Apply rapid updates using virtual DOM. This only works on the differences between the current DOM state and the target DOM state.

        For example, just change the light bulb without tearing down the house and building it back up.
    
    Store information in a component using states.

        For example, a traffic light is a component and its states are stop, wait and go.

Effect Hooks

    Allow us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes.

    Handling events with React elements is very similar to handling events on DOM elements. There are some syntax differences though.

        * React Events are named using camelCase, rather than lowercase.
        * With JSX you pass a function as the event handler, rather than a string.
    
    s52 instructor - Jino Yumul
    s54 instructor - Romenick Garcia






















npx create-react-app booking-app
npm start
npm install react-bootstrap bootstrap
npm install react-router-dom
npm install sweetalert2

import 'bootstrap/dist/css/bootstrap.min.css';