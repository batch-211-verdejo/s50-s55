// S53 activity
// import { Link } from 'react-router-dom';
// import { Button } from 'react-bootstrap';
import Banner from '../components/Banner';

export default function Error () {

    const  data = {
        title: "404 - Not found",
        content: "The page you are looking cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        // "prop" name is up to the developer
        <Banner bannerProp={data}/>
    )
}