// import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {Card, Button} from "react-bootstrap";


/* 
    1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card;
        1. the course name should be in the card title.
        2. The description and price should be in the card body.
    2. Render the CourseCard component in the Home page.    
*/

// Passing an object from Parent(Courses) to Child(CourseCard)

export default function CourseCard ({courseProp}) {

    // console.log({courseProp})

    // useState hook:
    // A hook in React is a kind of tool. The useState hook allows creation and manipulation of states

    // States are a way for React to keep track of any value and assocciate it with a component

    // When a state changes, React re-renders ONLY the specific component or part of the component that changed (and not the entire page or unrelated components whose states have not changed)

    // syntax:
    // const [state, setState] = useState(default state) [getter and setter]
    
    // state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed
    
    // State setter: State settes are the ONLY way to change a states' value. By convention, they are named after the state.
    
    // default state: The state's initial value on "page load"
    // Before a component mounts, a state actually defaults to undefined, THEN is changed to its default state

    // Component mounting??
    
    // array destructuring to get the state and the setter
    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(10);
    

    let {name, description, price, _id} = courseProp
    /* 
        ACTIVITY:
        Create a state hook that will represent the number of available seats for each course

        It should default to 10, and decrement by 1 each time a student enrolls

        Add a condition that will show an alert that no more seats are available if the seats state is 0.
    */

    // Refactor the enroll function instead the "useEffect" hooks
    /* function enroll() {
        
        // console.log("Enrollees:" + count)
        if (count !== 10) {
            setCount(count + 1);
            setSeats(seats - 1);
        } 
    } */

    // Apply the use effect hook
    // useEffect makes any given code block happen when a state changes AND when a componenet first mounts (such as on initial page load)

    // Syntax:
        // useEffect(function, [dependencies])
    
    /* useEffect(() => {
        if (seats === 0) {
            alert("No more seats available!")
        }
    }, [count, seats]); */
    

    
    

    return (
    
        <Card className="p-3 mb-3">
            <Card.Img variant="top" src="holder.js/100px180" />
            <Card.Body className="card-purple">
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>                    
                    {description}
                </Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>                        
                    PHP {price}
                </Card.Text>                
                <Button as={Link} to={`/courses/${_id}`}>details</Button>
            </Card.Body>
        </Card>
    
    )
}
